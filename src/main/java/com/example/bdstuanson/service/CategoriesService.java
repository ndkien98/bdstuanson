package com.example.bdstuanson.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bdstuanson.model.Categories;
import com.example.bdstuanson.respository.CategoriesRespository;

@Service
public class CategoriesService {

	@Autowired
	private CategoriesRespository repo;
	public List<Categories> listAll(){
		return repo.findAll();
	}
	
	public void save(Categories categories) {
		repo.save(categories);
	}
	
	public Categories get(Integer id) {
		return repo.findById(id).get();
	
	}
	public void delete(Integer id ) {
		repo.deleteById(id);
	}
}
