package com.example.bdstuanson.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bdstuanson.model.Categories;
import com.example.bdstuanson.service.CategoriesService;


@RestController
public class CategoriesController {

	@Autowired
	private CategoriesService service;

	@GetMapping("/category")
	public List<Categories> list() {
		return service.listAll();
	}

	@GetMapping("/category/{id}")
	public ResponseEntity< Categories> get(@PathVariable Integer id ) {
		try {
		Categories categories = service.get(id);
		return new ResponseEntity<Categories>(categories, HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<Categories>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/category")
	public void add(@RequestBody Categories categories ) {
		service.save(categories);
	}
	@PutMapping("/category/{id}")
	public ResponseEntity<?> update(@RequestBody Categories categories, @PathVariable Integer id ){
		try {
			Categories existCategory = service.get(id);
			service.save(categories);
			return new ResponseEntity<>(HttpStatus.OK);
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/category/{id}")
	public void  delete(@PathVariable Integer id ) {
		service.delete(id);
	}
	
	

}
