package com.example.bdstuanson.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bdstuanson.model.User;

@Repository
public interface UserRespository extends JpaRepository<User, Integer> {

}
