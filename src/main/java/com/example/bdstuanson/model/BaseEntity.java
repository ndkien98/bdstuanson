package com.example.bdstuanson.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public class BaseEntity implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 558158776181231035L;
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	@Id // xác định đây là khoá chính.
	@GeneratedValue(strategy = GenerationType.IDENTITY) // auto-increment.
	private Integer id; // primary-key

	@Column(name = "status", nullable = false)
	private Boolean status = Boolean.FALSE;

	@Column(name = "create_user_id")
	private Integer createUserId;

	@Column(name = "create_datetime")
	private LocalDateTime createDateTime;

	@Column(name = "update_user_id")
	private Integer updateUserId;

	@Column(name = "update_datetime")
	private LocalDateTime updateDateTime;

	public String getStringCreateDateTime() {
		if(createDateTime == null) return LocalDateTime.now().format(formatter);
		return createDateTime.format(formatter);
	}
	
	 @PrePersist
	    private void onInsert() {
	    	this.createDateTime = LocalDateTime.now();
	    }
	    
	    // ::: update
	    @PreUpdate
	    private void onUpdate() {
	    	this.updateDateTime = LocalDateTime.now();
	    }
		
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Boolean getStatus() {
			return status;
		}

		public void setStatus(Boolean status) {
			this.status = status;
		}

		public Integer getCreateUserId() {
			return createUserId;
		}

		public void setCreateUserId(Integer createUserId) {
			this.createUserId = createUserId;
		}

		public LocalDateTime getCreateDateTime() {
			return createDateTime;
		}

		public void setCreateDateTime(LocalDateTime createDateTime) {
			this.createDateTime = createDateTime;
		}

		public Integer getUpdateUserId() {
			return updateUserId;
		}

		public void setUpdateUserId(Integer updateUserId) {
			this.updateUserId = updateUserId;
		}

		public LocalDateTime getUpdateDateTime() {
			return updateDateTime;
		}

		public void setUpdateDateTime(LocalDateTime updateDateTime) {
			this.updateDateTime = updateDateTime;
		}
	
	

}
