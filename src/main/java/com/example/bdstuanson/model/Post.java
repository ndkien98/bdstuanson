package com.example.bdstuanson.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="t_post")
public class Post extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3645345471087033922L;

	@Column(name = "title", length = 500, nullable = false)
	private String title;
	@Column(name = "short_description", length = 500, nullable = false)

	private String shortDescription;
	@Column(name = "long_description", nullable = false, columnDefinition = "text")

	private String longDescription ;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}