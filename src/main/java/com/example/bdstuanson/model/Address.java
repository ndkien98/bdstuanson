package com.example.bdstuanson.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name ="t_address")
public class Address extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6341992725782563222L;
	
	@Column(name = "name", length = 45, nullable = false, unique = true)
    private String name;
	@Column(name = "description", length = 200, nullable = false)
	private String description;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
