package com.example.bdstuanson.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "t_contact")
public class Contact extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3352070318127476249L;
	@Column(name = "full_name", length = 200, nullable = false)

	private String fullName;
	@Column(name = "email", length = 45, nullable = false)

	private String email;
	@Column(name = "mobile", length = 45, nullable = false)

	private String mobile;
	@Column(name = "comment", length = 100, nullable = false)

	private String comment;
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
